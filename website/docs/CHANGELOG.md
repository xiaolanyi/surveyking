---
id: changelog
title: 更新日志
---

## v0.3.2(2022-06-29)

🆕 **新增功能**

- 答题完成后跳转自定义页面允许插入分数(考试)
- 答题完成后跳转自定义页面支持动态计算提示语
- 富文本编辑器添加 Tooltip

⛑ **问题修复**

- 题组导致答题卡和校验失败 [I5DCZO](https://gitee.com/surveyking/surveyking/issues/I5DCZO) 

⚡️ **体验优化**

- 优化编辑器性能，百道题的渲染时间由6s优化到1s
